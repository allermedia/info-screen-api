<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\BitbucketService;

/**
 * Class BitbucketController.
 *
 * @package App\Http\Controllers
 */
class BitbucketController extends Controller
{
    /**
     * Bitbucket Service.
     *
     * @var \App\Services\BitbucketService
     */
    protected $bitbucket;

    /**
     * BitbucketController constructor.
     *
     * @param \App\Services\BitbucketService $dsb
     */
    public function __construct(BitbucketService $bitbucket)
    {
        $this->bitbucket = $bitbucket;
    }

    /**
     * Get commits.
     *
     * @param string $username
     *     Username to retrieve latest commits from.
     * @param string project_key
     *     Project key to retrieve latest commits from.
     *
     * @return array
     */
    public function latestCommits($username, $project_key = null) : array
    {
        return [
            'data' => $this->bitbucket->latestCommits($username, $project_key)
        ];
    }
}
