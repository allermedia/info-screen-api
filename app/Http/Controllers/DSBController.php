<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\DSBService;

/**
 * Class DSBController.
 *
 * @package App\Http\Controllers
 */
class DSBController extends Controller
{
    /**
     * DSB Service.
     *
     * @var \App\Services\DSBService
     */
    protected $dsb;

    /**
     * DSBController constructor.
     *
     * @param \App\Services\DSBService $dsb
     */
    public function __construct(DSBService $dsb)
    {
        $this->dsb = $dsb;
    }

    /**
     * Get current S-Trains statuses.
     *
     * @return array
     */
    public function sTrains() : array
    {
        return [
            'data' => $this->dsb->sTrains()
        ];
    }

    /**
     * Get general DSB travel information
     *
     * @return array
     */
    public function traffic() : array
    {
        return [
            'data' => $this->dsb->trafficInformation()
        ];
    }
}
