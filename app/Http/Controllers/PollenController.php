<?php

namespace App\Http\Controllers;

use App\Services\PollenService;
use Illuminate\Http\Request;

class PollenController extends Controller
{
    /**
    * PollenService object.
    *
    * @var \App\Services\PollenService
    */
    protected $pollenService;

    /**
    * PollenController constructor.
    *
    * @param \App\Services\PollenService $pollenService
    */
    function __construct(PollenService $pollenService)
    {
        $this->pollenService = $pollenService;
    }

    public function getLatest()
    {
        return response()->json(['data' => $this->pollenService->getLatest()]);
    }
}