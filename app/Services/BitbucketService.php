<?php
declare(strict_types=1);

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\TransferException as GuzzleTransferException;
use Illuminate\Support\Collection;

/**
 * Class BitbucketService.
 *
 * @package App\Services
 */
class BitbucketService
{
    /**
     * Base URL for service.
     */
    const BASE_SERVICE_URL = 'https://api.bitbucket.org/2.0/';

    /**
     * HTTP Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * Bitbucket oAuth parameters.
     *
     * @var array
     */
    private $oauthParams = array(
        'client_id' => 'YxLwLA39hGzZB9MYFr',
        'client_secret' => 'RjTqBQNKxGPUjfAp6ZvEyjRubAZbmrAu',
    );

    /**
     * Bitbucket OAuth access token.
     *
     * @var string
     */
    private $accessToken;

    /**
     * BitbucketService constructor.
     */
    public function __construct()
    {
        $clientConfig = [
            'base_uri' => self::BASE_SERVICE_URL,
        ];
        $this->client = new GuzzleClient($clientConfig);
    }

    /**
     * Retrieve Bitbucket OAuth access token.
     */
    private function getAccessToken()
    {
        $json = NULL;

        // Retrieve new access token.
        if (!isset($this->accessToken)) {
            $response = $this->getClient()->post('https://bitbucket.org/site/oauth2/access_token', [
                'auth' => [
                    'YxLwLA39hGzZB9MYFr',
                    'RjTqBQNKxGPUjfAp6ZvEyjRubAZbmrAu',
                ],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                ],
            ]);
            $json = json_decode((string) $response->getBody());
        }

        // Refresh access token.
        if (isset($this->accessToken->expires) && $this->accessToken->expires < Carbon::now()) {
            $response = $this->getClient()->post('https://bitbucket.org/site/oauth2/access_token', [
                'auth' => [
                    'YxLwLA39hGzZB9MYFr',
                    'RjTqBQNKxGPUjfAp6ZvEyjRubAZbmrAu',
                ],
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $this->accessToken->refresh_token,
                ],
            ]);
            $json = json_decode((string) $response->getBody());
        }

        if (isset($json->access_token)) {
            $json->expires = Carbon::now()->addSeconds($json->expires_in);
            $this->accessToken = $json;
        }

        return $this->accessToken;
    }

    /**
     * Get HTTP Client instance.
     *
     * @return \GuzzleHttp\Client
     */
    public function getClient() : GuzzleClient
    {
        return $this->client;
    }

    /**
     * Get latest Bitbucket commits.
     *
     * @param string $username
     *     Username to retrieve latest commits from.
     * @param string project_key
     *     Project key to retrieve latest commits from.
     *
     * @return \Illuminate\Support\Collection
     */
    public function latestCommits($username, $project_key = null) : Collection
    {
        // Trains container.
        $commits = collect();

        try {
            $requestConfig = [
                'headers' => [
                    'Authorization' => sprintf('Bearer %s', $this->getAccessToken()->access_token),
                ],
                'query' => [
                    'sort' => '-updated_on',
                    'pagelen' => 5,
                ],
            ];

            if ($project_key) {
                $requestConfig['query']['q'] = 'project.key ~ "' . $project_key . '"';
            }
            $response = $this->getClient()->get(sprintf('repositories/%s', $username), $requestConfig);

            $repos = json_decode((string) $response->getBody());

            if (!empty($repos->values)) {
                foreach ($repos->values as $repo) {
                    $response = $response = $this->getClient()->get(sprintf('repositories/%s/%s/commits', $username, $repo->slug), [
                        'headers' => [
                            'Authorization' => sprintf('Bearer %s', $this->getAccessToken()->access_token),
                        ],
                        'query' => [],
                    ]);

                    $repoCommits = json_decode((string) $response->getBody());
                    if ($first = reset($repoCommits->values)) {
                        $commits->push([
                            'repo' => (string) $first->repository->name,
                            'author' => (string) $first->author->raw,
                            'message' => (string) $first->message,
                        ]);
                    }
                }
            }
        } catch (GuzzleTransferException $e) {
            // Fail silently.
        }

        return $commits;
    }
}
