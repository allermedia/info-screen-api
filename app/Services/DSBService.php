<?php
declare(strict_types=1);

namespace App\Services;

use Carbon\Carbon;
use DOMDocument;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\TransferException as GuzzleTransferException;
use Illuminate\Support\Collection;

/**
 * Class DSBService.
 *
 * @package App\Services
 */
class DSBService
{
    /**
     * Base URL for service.
     */
    const DSB_BASE_URL = 'http://www.dsb.dk';

    /**
     * HTTP Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * DSBService constructor.
     */
    public function __construct()
    {
        $this->client = new GuzzleClient;
    }

    /**
     * Get travel information about S-trains.
     *
     * @return \Illuminate\Support\Collection
     */
    public function sTrains() : Collection
    {
        // URL to S-train's travel information.
        $url = sprintf('%s/trafikinformation/s-tog-overblik/', self::DSB_BASE_URL);

        // Trains container.
        $trains = collect();

        try {
            // Fire request!
            $response = $this->getClient()->get($url);

            $dom = new DOMDocument;
            $dom->loadHTML((string) $response->getBody());

            $tables = $dom->getElementsByTagName("table");
            foreach ($tables as $table) {
                // Get all rows in <table>
                $rows = $table->getElementsByTagName('tr');
                foreach ($rows as $row) {
                    // Get all cells in <tr>
                    $cells = $row->getElementsByTagName('td');
                    if ($cells->length < 2) {
                        continue;
                    }

                    // Current train
                    $train = [];

                    foreach ($cells as $iteration => $cell) {
                        // Icon of train is always located in the first cell.
                        if ($iteration == 0) {
                            $train['icon'] = sprintf('%s%s', self::DSB_BASE_URL, $cell->getElementsByTagName('img')->item(0)->getAttribute('src'));
                        } else {
                            // Sometimes a status is available in danish and english
                            // which means, we have to loop through all <span>'s
                            // found in cell.
                            $statuses = $cell->getElementsByTagName('span');
                            foreach ($statuses as $status) {
                                if (empty($status->nodeValue)) {
                                    continue;
                                }
                                $train['text'][] = $status->nodeValue;
                            }
                        }
                    }

                    // Push current train to container
                    $trains->push($train);
                }
            }
        } catch (GuzzleTransferException $e) {
            // Fail silently.
        }

        return $trains;
    }

    public function trafficInformation()
    {
        // URL to traffic information feed.
        $url = sprintf('%s/rss-feeds/samlet-trafikinformation/', self::DSB_BASE_URL);

        // Data container.
        $data = collect();

        try {
            // Fire request!
            $response = $this->getClient()->get($url);

            // Load feed into SimpleXML.
            $feed = simplexml_load_string((string) $response->getBody());

            // Current time.
            $now = Carbon::now()->setTimezone('Europe/Copenhagen');

            foreach ($feed->channel->item as $item) {
                $validFrom = Carbon::parse((string) $item->validfrom)->setTimezone('Europe/Copenhagen');
                $validTo = Carbon::parse((string) $item->validto)->setTimezone('Europe/Copenhagen');

                // We only want return entries
                // that are currently in effect.
                if (!$now->between($validFrom, $validTo)) {
                    continue;
                }

                // Add to data container, but not before
                // running through the array and trim each value.
                $data->push([
                    'title' => trim((string) $item->title),
                    'type' => str_replace(['Dagens', 'Planlagt'], ['daily', 'planned'], trim((string) $item->messagetype)),
                    'regarding' => trim((string) $item->sender),
                    'description' => trim((string) $item->description),
                    'link' => trim((string) $item->link),
                    'valid' => [
                        'from' => $validFrom->toIso8601String(),
                        'to' => $validFrom->toIso8601String(),
                    ],
                    'published' => Carbon::parse($item->pubDate)->toIso8601String()
                ]);
            }
        } catch (GuzzleTransferException $e) {
            // Fail silently.
        }

        return $data;
    }

    /**
     * Get HTTP Client instance.
     *
     * @return \GuzzleHttp\Client
     */
    public function getClient() : GuzzleClient
    {
        return $this->client;
    }
}