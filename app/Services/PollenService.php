<?php

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Nathanmac\Utilities\Parser\Facades\Parser;

class PollenService
{
    /**
     * @var \Carbon\Carbon
     */
    protected $dateParser;

    /**
     * @var \Nathanmac\Utilities\Parser\Facades\Parser
     */
    protected $dataParser;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * PollenService constructor.
     *
     * @param \Carbon\Carbon $dateParser
     * @param \Nathanmac\Utilities\Parser\Parser $parser
     * @param \GuzzleHttp\Client $client
     */
    function __construct(Carbon $dateParser, Parser $dataParser, Client $client)
    {
        $this->dateParser = $dateParser;
        $this->dataParser = $dataParser;
        $this->client = $client;
    }

    /**
     * Get latest pollen cast.
     *
     * @return array
     */
    public function getLatest()
    {
        $res = $this->client->request('GET', 'https://twitrss.me/twitter_user_to_rss/?user=AstmaAllergiDK');

        if ($res->getStatusCode() == '200')
        {
            $xml = (string) $res->getBody();
            $parsed = $this->dataParser::xml($xml);
            $latestPollen = NULL;
            if (isset($parsed['channel']['item']))
            {
                foreach ($parsed['channel']['item'] as $item)
                {
                    if (stripos($item['title'], 'dagens pollental') !== FALSE)
                    {
                        $latestPollen = $item;
                        break;
                    }
                }

                if ($latestPollen)
                {
                    $date = Carbon::parse($latestPollen['pubDate']);
                    $areas = explode("\n\n", $latestPollen['title']);
                    $title = array_shift($areas);
                    foreach ($areas as &$area)
                    {
                        $deconstructed = explode("\n", $area);
                        $areaTitle = array_shift($deconstructed);
                        $area = [
                            'area' => $areaTitle,
                            'info' => $deconstructed,
                        ];
                    }
                    return [
                        'title' => $title,
                        'areas' => $areas,
                        'pubDate' => $date->toIso8601String(),
                    ];
                }
            }
        }

        return [];
    }
}