<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return 'DK Info Screen API';
});

$app->group(['prefix' => 'pollen'], function () use ($app) {
    $app->get('latest', [
        'as' => 'pollen.latest',
        'uses' => 'PollenController@getLatest',
    ]);
});

// DSB Service
$app->group(['prefix' => 'dsb'], function() use ($app) {
    $app->get('/s-trains', [
        'as' => 'dsb.s-trains',
        'uses' => 'DSBController@sTrains'
    ]);

    $app->get('/traffic', [
        'as' => 'dsb.traffic',
        'uses' => 'DSBController@traffic'
    ]);
});

// Bitbucket
$app->group(['prefix' => 'bitbucket'], function() use ($app) {
    $app->get('/latest-commits/{username}[/{project_key}]', [
        'as' => 'bitbucket.latest-commits',
        'uses' => 'BitbucketController@latestCommits'
    ]);
});
